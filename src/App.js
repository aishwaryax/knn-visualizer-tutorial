import React from 'react';
import { Nav, Navbar, NavDropdown } from 'react-bootstrap'
import './App.css';

class App extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
        color: null,
        isTraining: false,
        isPredicting: false,
        input: [],
        k: 5,
        n: 3, 
        colorCodes: ['red', 'blue', 'green'],
        modalShow: false
    }
  }

  //knn helpers

  euclideanDistance (P1, P2) {
    return Math.sqrt(Math.pow(P1.X - P2.X, 2) + Math.pow(P1.Y - P2.Y, 2))
  } 

  getNeighbors(posx, posy) {
    var distances = []
    for (let input of this.state.input) {
      var elem = {neighbor: input, distance: this.euclideanDistance(input, {X: posx, Y: posy})}
      distances = [...distances, elem]
      distances.sort(function(a, b){return a.distance - b.distance});
    }
    return distances
  }

  knn (posx, posy) {
    var neighbors = this.getNeighbors(posx, posy)
    var outputs = []
    for(let i in this.state.colorCodes) {
      var output = {color: this.state.colorCodes[i], n: 0}
      outputs = [...outputs, output]
    }
    var nearestNeighbors = neighbors.slice(0, this.state.k)
    for (let i in nearestNeighbors) {
      outputs[nearestNeighbors[i].neighbor.label].n += 1
    }
    outputs.sort(function(first, second) {
      return second.n - first.n;
    })
    console.log(outputs)
    return outputs[0].color
  }
  draw = async (event) => {
    var canvas = document.getElementById("canvas")
    var context = canvas.getContext("2d");
    var pos = this.getMousePos(canvas, event)
    var posx = pos.x
    var posy = pos.y
    if (this.state.isTraining && this.state.color !== null) {
    context.fillStyle = this.state.color
    context.beginPath()
    context.arc(posx, posy, 9, 0, 2*Math.PI)
    this.setState({input: [...this.state.input, {X: posx, Y: posy, label: this.state.colorCodes.indexOf(this.state.color)}]})
    context.fill()
  }
  else if (this.state.isPredicting) {
    var color = await this.knn(posx, posy)
    context.fillStyle = color
    context.beginPath()
    context.arc(posx, posy, 9, 0, 2*Math.PI)
    context.fill()
    this.setState({outputColor: null})
  }
  }

  getMousePos(canvas, event) {
    var rect = canvas.getBoundingClientRect();
    return {
      x: event.clientX - rect.left,
      y: event.clientY - rect.top
    }
  }





  render() {
    return (
      <>
        <Navbar bg="light" expand="lg">
         <Navbar.Brand href="/">KNN Visualizer</Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
         <Nav className="mr-auto">
         <Nav.Link onClick={() => {this.setState({isTraining: true})}}>Start Training </Nav.Link>
         <Nav.Link onClick={() => {this.setState({isTraining: false})}}>Stop Training </Nav.Link>
         <Nav.Link onClick={() => {this.setState({isPredicting: true})}}>Predicting </Nav.Link>
         <NavDropdown title="Choose Color" id="basic-nav-dropdown">
         <NavDropdown.Item onClick={() => {this.setState({color: "red"})}}>Red</NavDropdown.Item>
         <NavDropdown.Item onClick={() => {this.setState({color: "blue"})}}>Blue</NavDropdown.Item>
         <NavDropdown.Item onClick={() => {this.setState({color: "green"})}}>Green</NavDropdown.Item>
         </NavDropdown>
         </Nav>
         </Navbar.Collapse>
        </Navbar>
        <canvas id="canvas" 
          ref="canvas" 
          width={window.innerWidth} 
          height={window.innerHeight}
          onClick={e => {let nativeEvent = e.nativeEvent; this.draw(nativeEvent);}}/>
      </>
    );  
  }
}

export default App;
